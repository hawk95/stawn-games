﻿using UnityEngine;
using System.Collections;

public class AutoDestroyObject : MonoBehaviour
{
    public float DestroyDelay = 2f;
    public GameObject DestoryEffect;

    void Update()
    {

    }
    public void Controller2DEnter(PlayerController2D controller)
    {
        if (DestoryEffect != null)
        {
            Instantiate(DestoryEffect, transform.position, transform.rotation);
        }
        Destroy(gameObject, DestroyDelay);
    }
}
